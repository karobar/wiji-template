package screens;

import com.badlogic.gdx.Input;
import com.google.common.collect.ImmutableSet;
import tjp.wiji.drawing.BitmapContext;
import tjp.wiji.event.GameEvent;
import tjp.wiji.gui.GUItext;
import tjp.wiji.gui.GuiElement;
import tjp.wiji.gui.screen.Screen;
import tjp.wiji.gui.screen.ScreenChangeController;
import tjp.wiji.representations.ImageRepresentation;

import java.util.Collection;

public class SampleBlankScreen extends Screen {
    protected SampleBlankScreen(ScreenChangeController screenChangeController) {
        super(screenChangeController);
    }

    @Override
    protected Collection<GuiElement> getGuiElements() {
        return ImmutableSet.of(new GUItext("SampleScreen", 5, 5));
    }

    @Override
    protected ImageRepresentation getCurrentCell(int i, int i1) {
        return ImageRepresentation.ALL_BLACK;
    }

    @Override
    protected void handleFrameChange() {

    }

    @Override
    public void stepToScreenTrigger() {

    }

    @Override
    public void handleEvent(GameEvent gameEvent) {
        if (gameEvent.getIntCode() == Input.Keys.ENTER) {
            screenChangeController.stepScreenBackwards();
        }
    }
}
