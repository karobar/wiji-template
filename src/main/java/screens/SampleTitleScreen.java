package screens;

import com.badlogic.gdx.Input;
import com.google.common.collect.ImmutableSet;
import tjp.wiji.event.GameEvent;
import tjp.wiji.gui.GuiElement;
import tjp.wiji.gui.screen.Screen;
import tjp.wiji.gui.screen.ScreenChangeController;
import tjp.wiji.representations.ImageRepresentation;

import java.util.Collection;

public class SampleTitleScreen extends Screen {
    public SampleTitleScreen(final ScreenChangeController screenChangeController) {
        super(screenChangeController);
    }

    @Override
    protected Collection<GuiElement> getGuiElements() {
        return ImmutableSet.of();
    }

    @Override
    protected ImageRepresentation getCurrentCell(int i, int i1) {
        return ImageRepresentation.ERROR;
    }

    @Override
    protected void handleFrameChange() {

    }

    @Override
    public void stepToScreenTrigger() {

    }

    @Override
    public void handleEvent(final GameEvent gameEvent) {
        if (gameEvent.getIntCode() == Input.Keys.ENTER) {
            screenChangeController.stepScreenForwards(new SampleBlankScreen(screenChangeController));
        }
    }
}
