package main;

import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.Subject;
import screens.SampleTitleScreen;
import tjp.wiji.drawing.ApplicationFactory;
import tjp.wiji.gui.screen.Screen;
import tjp.wiji.gui.screen.ScreenChangeController;
import tjp.wiji.gui.screen.ScreenContext;

public class SampleMain {
    private static ScreenContext makeScreenContext() {
        final Subject<Boolean> backStream = BehaviorSubject.create();
        final Subject<Screen> forwardStream = BehaviorSubject.create();

        final ScreenChangeController controller = new ScreenChangeController() {
            @Override
            public void stepScreenBackwards() {
                backStream.onNext(true);
            }

            @Override
            public void stepScreenForwards(Screen screen) {
                forwardStream.onNext(screen);
            }
        };

        final Screen sampleScreen = new SampleTitleScreen(controller);
        final ScreenContext screenContext = ScreenContext.create(sampleScreen);

        backStream.subscribe((ignored) -> screenContext.stepScreenBackwards());
        forwardStream.subscribe(screenContext::stepScreenForwards);

        return screenContext;
    }

    public static void main(final String[] args) {
        ApplicationFactory.runApp(makeScreenContext());
    }
}
